import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import AppNavigator from './AppNavigator';
import { createAppContainer } from 'react-navigation';
import promise from 'redux-promise-middleware';
import { combineReducers } from 'redux';
import { restaurantsReducer } from './src/components/home/RestaurantReducers';
import { splashReducer } from './src/components/splash/SplashReducer';
import { briefInfoReducer } from './src/components/brief-info/BriefInfoReducers';
import { mapsReducer } from './src/components/maps/MapsUtilityReducer';

const appReducer = combineReducers({
  location: splashReducer,
  restaurants: restaurantsReducer,
  briefInfo: briefInfoReducer,
  maps: mapsReducer
});

export default class App extends React.Component {
  

  render() {
    const middleware = [promise];
    const store = createStore(appReducer, applyMiddleware(...middleware));
    const AppContainer = createAppContainer(AppNavigator);
    return (
      <Provider store={ store }>
        <AppContainer />
      </Provider>
    );
  }
}
