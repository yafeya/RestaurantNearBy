import { createStackNavigator } from 'react-navigation';
import Home from './src/components/home/Home';
import Splash from './src/components/splash/Splash';
import BriefInfo from './src/components/brief-info/BriefInfo';
import MapsUtility from './src/components/maps/MapsUtility';

const AppNavigator = createStackNavigator(
  {
    Home: Home,
    Splash: Splash,
    Brief: BriefInfo,
    Maps: MapsUtility
  },
  {
    initialRouteName: 'Splash',
  });

export default AppNavigator;
