package com.restaurantnearby;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;

public class UtilMap extends ReactContextBaseJavaModule {

    public UtilMap(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "UtilMap";
    }

    /*
     * 检测手机是否安装了相应的地图app。返回的数据格式为:[{title:'dadasda'url:app地图URL},{title:'dadasda'url
     * :app地图URL},{title:'dadasda'url:app地图URL}]
     */
    @ReactMethod
    public void findEvents(String baidu_des_lon, String baidu_des_lat, String amap_des_lon, String amap_des_lat,
            Callback successCallback) throws Exception {

        WritableArray array = new WritableNativeArray();

        // 百度地图app检测
        if (isAppInstalled(getReactApplicationContext(), "com.baidu.BaiduMap")) {
            WritableNativeMap ob = new WritableNativeMap();
            ob.putString("title", "百度地图");
            ob.putString("url", "baidumap://map/direction?origin=我的位置&destination=" + baidu_des_lat + ","
                    + baidu_des_lon + "&coord_type=bd09ll&mode=walking&src=com.restaurantnearby");
            array.pushMap(ob);
        }

        // 高德地图app检测
        if (isAppInstalled(getReactApplicationContext(), "com.autonavi.minimap")) {
            WritableNativeMap ob = new WritableNativeMap();
            ob.putString("title", "高德地图");
            ob.putString("url", "amapuri://route/plan/?sourceApplication=com.restaurantnearby&sname=我的位置&dlat="
                    + amap_des_lat + "&dlon=" + amap_des_lon + "&t=2");
            array.pushMap(ob);
        }

        // 腾讯地图app检测
        // if (isAppInstalled(getReactApplicationContext(), "com.tencent.map")) {
        // WritableNativeMap ob = new WritableNativeMap();
        // ob.putString("title", "腾讯地图");
        // ob.putString("url", "qqmap://map/routeplan?from=我的位置&type=drive&tocoord=" +
        // lat + "," + lon + "&to="
        // + address + "&coord_type=1&policy=0");
        // array.pushMap(ob);
        // }

        WritableNativeMap ob = new WritableNativeMap();
        ob.putString("title", "取消");
        ob.putString("url", "");
        array.pushMap(ob);

        successCallback.invoke(array);

    }

    @ReactMethod
    public void addEvent(String url) {
        // 打开对应的app
        if (!url.equals("")) {
            Intent i1 = new Intent();
            i1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i1.setData(Uri.parse(url));
            getReactApplicationContext().startActivity(i1);
            i1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }

    }

    /**
     * 查看是否安装了这个导航软件 高德地图 com.autonavi.minimap 百度地图 com.baidu.BaiduMap 腾讯地图
     * com.tencent.map
     *
     * @param context
     * @param packagename
     * @return
     */
    public boolean isAppInstalled(Context context, String packagename) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packagename, 0);
        } catch (Exception e) {
            packageInfo = null;
            e.printStackTrace();
        }

        if (packageInfo == null) {
            return false;
        } else {
            return true;
        }
    }
}