import React from 'react';
import { setCandidateRestaurants, setSelectedRestaurant } from './BriefInfoActions';
import { View, Text, Button, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


class BriefInfo extends React.Component { 
    onSelectOther(e) { 
        this.setCurrentSelection();
    }

    setCurrentSelection() {
        let current = this.props.briefInfo.selected;
        if (current && current.name && current.name !== '') {
            let currentIndex = this.props.briefInfo.candidates.indexOf(current);
            if (currentIndex >= 0) {
                this.props.briefInfo.candidates.splice(currentIndex, 1);
            }
        }
        let randomNumber = Math.round(Math.random() * this.props.briefInfo.candidates.length);
        let newSelected = this.props.briefInfo.candidates[randomNumber];
        this.props.setSelectedRestaurant(newSelected);
    }

    render() {
        return (
            <View style={styles.container}>
                {this.props.briefInfo.selected && <Text>{this.props.briefInfo.selected.name}</Text>}
                {this.props.briefInfo.selected &&<Text>{this.props.briefInfo.selected.address}</Text>}
                
                <Button title="不感兴趣" style={styles.buttonColor} onPress={e => this.onSelectOther(e)}></Button>
                <Button title="查看详情" style={styles.buttonColor}></Button>
            </View>
        );
    }

    componentDidMount() {
        this.props.setCandidateRestaurants(this.props.restaurants.candidates);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: 'center'
    },
    buttonColor: {
        color: '#1189f4'
    }
});

const mapStateToProps = (state) => {
    const { restaurants, briefInfo } = state;
    return { restaurants, briefInfo };
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        setCandidateRestaurants,
        setSelectedRestaurant
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(BriefInfo);