import { SET_CURRENT_RESTAURANT, SET_CANDIDATE_RESTAURANTS } from "./BriefInfoConsts";

export const setCandidateRestaurants = restaurants => ({
    type: SET_CANDIDATE_RESTAURANTS,
    payload: restaurants
});

export const setSelectedRestaurant = restaurant => ({
    type: SET_CURRENT_RESTAURANT,
    payload: restaurant
});