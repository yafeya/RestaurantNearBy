import { SET_CANDIDATE_RESTAURANTS, SET_CURRENT_RESTAURANT } from "./BriefInfoConsts";

const INITIAL_STATE = {
    candidates: [],
    selected: {}
};

selectRandomRestaurant = function (candidates) { 
    let randomNumber = Math.round(Math.random() * candidates.length);
    let newSelected = candidates[randomNumber];
    return newSelected;
}

export const briefInfoReducer = (state = INITIAL_STATE, action) => {
    let result = {};
    switch (action.type) {
        case SET_CANDIDATE_RESTAURANTS:
            result = {
                candidates: action.payload,
                selected: selectRandomRestaurant(action.payload)
            };
            break;
        case SET_CURRENT_RESTAURANT:
            result = {
                ...state,
                selected: action.payload
            };
            break;
        default:
            result = state;
            break;
    }
    return result;
};
