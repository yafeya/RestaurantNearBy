import React,{Component} from 'react';
import { StyleSheet, Text, View, Button, BackHandler, Alert } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Dropdown } from 'react-native-material-dropdown';
import { updateSelectedPriceSection } from './RestaurantActions';


export  class PricePicker extends Component {
    render() {
          return (
              <View>
                  <Dropdown
                      label='select a price section'
                      data={this.props.priceSectionOptions}
                      onChangeText ={this.props.updateSelectedPriceSection}
                  />
              </View>
          );
    }
};

const mapStateToProps = (state) => {
    const { restaurants} = state;
    return restaurants;
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        updateSelectedPriceSection
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(PricePicker);
