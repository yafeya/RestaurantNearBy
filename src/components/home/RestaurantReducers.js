import {
    FETCH_RESTAURANTS_PENDING,
    FETCH_RESTAURANTS_FULFILLED,
    FETCH_RESTAURANTS_REJECTED,
    UPDATE_PRICE_SECTION
} from './RestaurantConsts';
import { Location, Restaurant } from '../../models/models';


const INITIAL_STATE = {
    candidates: [],
    dataFetched: false,
    isFetching: false,
    error: false,
    selectedPriceSection: [20,50],
    priceSectionOptions: [{
        label: '10-20',  value:[10,20]
        }, {
        label: '20-50',   value:[20,50]
        }, {
        label: '50-100',   value:[50,100]
        }]
};

function toRestaurants(jArray) {
    let restaurants = [];
    try {
        for (let index = 0; jArray.length; index++) {
            let id = index + 1;
            let j_item = jArray[index];
            let location = new Location(j_item.location.latitude, j_item.location.longitude);
            let restaurant = new Restaurant();
            restaurant.id = id;
            restaurant.address = j_item.address            ;
            restaurant.city = j_item.city;
            restaurant.distance = j_item.distance;
            restaurant.location = location;
            restaurant.name = j_item.name;
            restaurant.price = j_item.price;
            restaurant.province = j_item.province;
            restaurant.rating = j_item.rating;
            restaurant.time = j_item.time;
            restaurants.push(restaurant);
        }
     }
    catch (error) {
        console.log(error);
    }
    return restaurants;
}

export const restaurantsReducer = (state = INITIAL_STATE, action) => {
    let result = {};
    switch (action.type) {
        case FETCH_RESTAURANTS_PENDING:
            result = {
                ...state,
                candidates: [],
                isFetching: true
            };
            break;
        case FETCH_RESTAURANTS_FULFILLED:
            result = {
                ...state,
                candidates: toRestaurants(action.payload),
                isFetching: false,
                dataFetched: true
            };
            console.log('Fetched restaurants Successfully');
            break;
        case FETCH_RESTAURANTS_REJECTED:
            result = {
                ...state,
                isFetching: false,
                error: true
            };
            console.log('Error Happened during fetching restaurants');
            break;
        case UPDATE_PRICE_SECTION:
            result= {
                    ...state,
                    selectedPriceSection:action.selectedPriceSection
                 };
            break;
        default:
            result = state;
            break;
    }
    return result;
};
