import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchCurrentLocation } from '../splash/SplashActions';
import { fetchMapItems } from './MapsUtilityActions';
import { navigateToDestinationByMapUrl, baiduToAmapCoordinate } from './MapsUtilityApi';
import { Location } from '../../models/models';

class MapsUtility extends React.Component {

    openNativeMap(baiduMapDestination, amapDestination) {
        this.props.fetchMapItems(baiduMapDestination, amapDestination);
    }

    onNavWithMap(item) { 
        navigateToDestinationByMapUrl(item.workingUrl);
    }

    onOpen3rdPartyMap() {
        let baiduDestination = new Location(39.924536, 116.359475);
        let amapDestination = baiduToAmapCoordinate(baiduDestination);
        this.openNativeMap(baiduDestination, amapDestination);
    }

    render() {
        if (this.props.location.error) { 
            alert('Errors, please check your GPS Settings.');
        }

        if (this.props.maps.error) { 
            alert('Errors, fetch maps error.');
        }

        let mapItems;
        if (this.props.maps.dataFetched) {
            mapItems = this.props.maps.mapItems
                .map(item => <Button key={item.title} title={item.title}
                                     onPress={() => this.onNavWithMap(item)} 
                                     color="#841584"></Button>);
        }

        return (
            <View style={styles.container}>
                <Text>Map Util</Text>
                {this.props.maps.isFetching && <Text>Fetching Maps...</Text>}
                {this.props.maps.dataFetched && <View>{mapItems}</View>}
            </View>
        )
    }

    componentDidMount() {
        this.onOpen3rdPartyMap();
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

const mapStateToProps = (state) => {
    const { location, maps } = state;
    return { location, maps };
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        fetchCurrentLocation,
        fetchMapItems
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(MapsUtility);