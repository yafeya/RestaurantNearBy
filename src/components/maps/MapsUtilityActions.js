import { fetchInstalledMapItems } from './MapsUtilityApi';
import { FETCH_MAPS } from './MapsUtilityConsts';

export const fetchMapItems = (baiduDestination, ampaDestination) => ({
    type: FETCH_MAPS,
    payload: fetchInstalledMapItems(baiduDestination, ampaDestination)
});