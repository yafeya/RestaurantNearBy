import { NativeModules } from 'react-native';
import { MapUtilityItem, Location } from '../../models/models';

let UtilMapManager = NativeModules.UtilMap;

export function fetchInstalledMapItems(baiduDestination, ampaDestination) {
    return new Promise((resolve, reject) => {
        try {
            let bmap_lon = baiduDestination.longitude.toString();
            let bmap_lat = baiduDestination.latitude.toString();
            let amap_lon = ampaDestination.longitude.toString();
            let amap_lat = ampaDestination.latitude.toString();
            UtilMapManager.findEvents(bmap_lon, bmap_lat, amap_lon, amap_lat, (events) => {
                let mapItems = [];
                events.map((item, index) => {
                    let mapItem = new MapUtilityItem();
                    mapItem.title = item.title;
                    mapItem.workingUrl = item.url;
                    mapItems.push(mapItem);
                });
                resolve(mapItems);
            });
        }
        catch (error) {
            reject(error);
        }
    });
}

export function navigateToDestinationByMapUrl(url) {
    UtilMapManager.addEvent(url);
}

export function baiduToAmapCoordinate(baiduCoordinate) {
    var X_PI = Math.PI * 3000.0 / 180.0;
    var x = baiduCoordinate.longitude - 0.0065;
    var y = baiduCoordinate.latitude - 0.006;
    var z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * X_PI);
    var theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * X_PI);
    var gg_lng = z * Math.cos(theta);
    var gg_lat = z * Math.sin(theta);
    var location = new Location(gg_lat, gg_lng);
    return location;
}