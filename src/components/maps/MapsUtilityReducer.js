import {
    FETCH_MAPS_PENDING,
    FETCH_MAPS_FULFILLED,
    FETCH_MAPS_REJECTED
} from './MapsUtilityConsts';


const INITIAL_STATE = {
    mapItems: [],
    dataFetched: false,
    isFetching: false,
    error: false
};

export const mapsReducer = (state = INITIAL_STATE, action) => {
    let result = {};
    switch (action.type) {
        case FETCH_MAPS_PENDING:
            result = {
                ...state,
                mapItems: [],
                isFetching: true,
                dataFetched: false
            };
            break;
        case FETCH_MAPS_FULFILLED:
            result = {
                ...state,
                mapItems: action.payload,
                isFetching: false,
                dataFetched: true
            };
            console.log('Fetched Maps Successfully');
            break;
        case FETCH_MAPS_REJECTED:
            result = {
                ...state,
                isFetching: false,
                error: true
            };
            console.log('Error Happened during fetching Maps');
            break;
        default:
            result = state;
            break;
    }
    return result;
};
