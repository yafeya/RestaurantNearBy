import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchCurrentLocation } from './SplashActions';

class Splash extends React.Component {
    render() {
        if (!this.props.location.isFetching) {
            if (this.props.location.dataFetched) {
                this.props.navigation.navigate('Home');
            }
            else if (this.props.location.error) { 
                alert('Errors, please check your network.');
            }
        }
        return (
            <View style={styles.container}>
                <Text>饭否</Text>
            </View>
        )
    }

    componentDidMount() {
        this.props.fetchCurrentLocation();
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

const mapStateToProps = (state) => {
    const { location } = state;
    return { location };
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        fetchCurrentLocation
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Splash);