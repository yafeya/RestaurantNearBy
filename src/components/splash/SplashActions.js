import { FETCH_LOCATION } from './SplashConsts'
import { get_current_location } from './SplashApi'

export const fetchCurrentLocation = () => ({
    type: FETCH_LOCATION,
    payload: get_current_location()
})