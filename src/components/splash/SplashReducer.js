import { FETCH_LOCATION_PENDING, FETCH_LOCATION_FULFILLED, FETCH_LOCATION_REJECTED } from './SplashConsts';

const INITIAL_STATE = {
    location: '',
    dataFetched: false,
    isFetching: false,
    error: false
};

export const splashReducer = (state = INITIAL_STATE, action) => {
    let result = {};
    switch (action.type) {
        case FETCH_LOCATION_PENDING:
            result = {
                ...state,
                location: '',
                isFetching: true
            };
            break;
        case FETCH_LOCATION_FULFILLED:
            result = {
                ...state,
                location: action.payload,
                isFetching: false,
                dataFetched: true
            };
            console.log('Fetched location Successfully');
            break;
        case FETCH_LOCATION_REJECTED:
            result = {
                ...state,
                isFetching: false,
                error: true
            };
            console.log('Error Happened during fetching location');
            break;
        default:
            result = state;
            break;
    }
    return result;
};